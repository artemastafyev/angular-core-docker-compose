using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webapi.Model;

namespace webapi.Model
{
    public class HeroInitializer
    {
        private ApplicationContext _context;

        public HeroInitializer(ApplicationContext context)
        {
            _context = context;
        }

        public async Task Seed()
        {
            await _context.Database.EnsureCreatedAsync();

            if (!_context.Heroes.Any())
            {
                _context.Heroes.AddRange(_heroes);
                await _context.SaveChangesAsync();
            }
        }
        
        List<Hero> _heroes = new List<Hero>
        {
            new Hero() { id = 44, name = "Mr. White" },
            new Hero() { id = 55, name = "Mr. Blue" },
            new Hero() { id = 66, name = "Mr. Pink" },
            new Hero() { id = 77, name = "Mr. Black" },
            new Hero() { id = 88, name = "Mr. Gray" },
            new Hero() { id = 99, name = "Mr. Green" }
        };
    }
}