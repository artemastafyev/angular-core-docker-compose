﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webapi.Services;

using webapi.Model;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    public class HeroesController : Controller
    {
        IHeroesService _heroesService;

        public HeroesController(IHeroesService heroesService)
        {
            _heroesService = heroesService;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<Hero>> Get()
        {
            return await _heroesService.GetHeroes();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<Hero> Get(int id)
        {
            return await _heroesService.GetHero(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
